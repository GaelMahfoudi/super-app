package gael.com.superapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;

/**
 * Created by aurelien on 10/05/16.
 */
@RunWith(AndroidJUnit4.class)
public class IntegratedTest {

    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void isTheFabDisplayed() {
        onView(withId(R.id.fab)).check(matches(isDisplayed()));
    }
}
