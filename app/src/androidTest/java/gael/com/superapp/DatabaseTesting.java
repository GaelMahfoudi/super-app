package gael.com.superapp;

import android.support.test.rule.ActivityTestRule;
import android.test.IsolatedContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;


import gael.com.superapp.database.DatabaseHelper;
import gael.com.superapp.database.UserDao;
import gael.com.superapp.model.User;

/**
 * Created by aurelien on 10/05/16.
 */
public class DatabaseTesting {

    @Rule
    public ActivityTestRule<MainActivity> testRule = new ActivityTestRule<>(MainActivity.class);

    public UserDao userDao;

    @Before
    public void init() {
        testRule.getActivity().deleteDatabase("testDb");
        DatabaseHelper dbHelper = new DatabaseHelper(testRule.getActivity(), "testDb");
        userDao = new UserDao(dbHelper.getUserDao());
    }


    @Test
    public void isSavingUserCorrectly() {
        User testUser = new User("Paul", 10);
        userDao.saveUser(testUser);

        assertThat(userDao.getUserByName("Paul"), is(not(nullValue())));
        assertThat(userDao.getAllUsers().size(), is(equalTo(1)));
    }


    @After
    public void clean() {
        testRule.getActivity().deleteDatabase("testDb");
    }
}
