package gael.com.superapp;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * Created by aurelien on 10/05/16.
 */
public class SomeTest {

    @Test
    public void isTwoPlusTwoEqualToFor() {
        assertThat(2+2, is(equalTo(4)));
    }
}
