package gael.com.superapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by aurelien on 10/05/16.
 */
@DatabaseTable(tableName = "USER")
public class User {

    @DatabaseField(columnName = "USER_AGE")
    private int age;
    @DatabaseField(columnName = "USER_NAME")
    private String name;
    @DatabaseField(columnName = "USER_ID", generatedId = true)
    private int id;

    public User() {
    }

    public User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
