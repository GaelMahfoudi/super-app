package gael.com.superapp.database;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

import gael.com.superapp.model.User;

/**
 * Created by aurelien on 10/05/16.
 */
public class UserDao {

    private RuntimeExceptionDao<User, Integer> userDao;


    public UserDao(RuntimeExceptionDao dao) {
        userDao = dao;
    }


    public boolean saveUser(User userToSave) {
        int result = userDao.create(userToSave);
        return (result == 1);
    }

    public boolean delUser(User userToDel) {
        int result = userDao.delete(userToDel);
        return (result == 1);
    }

    public List<User> getUserByName(String name) {
        return userDao.queryForEq("USER_NAME", name);
    }

    public List<User> getAllUsers() {
        return userDao.queryForAll();
    }
}
