package gael.com.superapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import gael.com.superapp.R;
import gael.com.superapp.model.User;

/**
 * Created by aurelien on 10/05/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private RuntimeExceptionDao<User, Integer> userDao;


    public DatabaseHelper(Context context, String databaseName) {
        super(context, databaseName, null, DATABASE_VERSION, R.raw.ormlite_config);
    }


    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.i("DatabaseHelper", "Creating USER table");
            TableUtils.createTable(connectionSource, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i("DatabaseHelper", "Deleting USER table");
            TableUtils.dropTable(connectionSource, User.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public RuntimeExceptionDao getUserDao() {
        if (userDao == null) {
            userDao = getRuntimeExceptionDao(User.class);
        }
        return userDao;
    }

    @Override
    public void close() {
        super.close();
    }
}
